#include "stdafx.h"
#include "./DbActionLogic.h"

#include "../Lib_Network/s_CSessionServer.h"
#include "../Lib_Network/s_CFieldServer.h"
#include "../Lib_Network/s_CAgentServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CGetPoints::CGetPoints(
    int   nChaNum,
	DWORD dwUserID,
	DWORD dwClientID )
	: m_nChaNum( nChaNum )
	, CDbAction( dwClientID )
{
	m_dwClient = dwClientID;
	m_dwUserID = dwUserID;
}

int CGetPoints::Execute( CServer* pServer )
{
	
	GLMSG::SNETPC_RETRIEVE_POINTS_FB NetMsgFB;
	int PPoints = 0, VPPoints = 0;
	int nResult = m_pDbManager->GetPoints( m_nChaNum, m_dwUserID , m_dwClient , &PPoints , &VPPoints );

	if ( nResult != DB_ERROR )
	{
		NetMsgFB.emFB		 = EMREQ_RETRIEVE_POINTS_FB_OK;
		NetMsgFB.PPoints		 = PPoints;
		NetMsgFB.VPoints		 = VPPoints;
	}
	else
	{
		NetMsgFB.emFB		 = EMREQ_RETRIEVE_POINTS_FB_FAIL;
	}

	CFieldServer* pTemp = reinterpret_cast<CFieldServer*> (pServer);
	pTemp->InsertMsg ( m_dwClient, (char*) &NetMsgFB );

	return nResult;
}

CGetItemShop::CGetItemShop(
	DWORD dwCharID )
{
	m_dwCharID = dwCharID;
}

int CGetItemShop::Execute(
	CServer* pServer )
{
    m_pDbManager->GetItemShop( m_vItem );

	PGLCHAR pChar = GLGaeaServer::GetInstance().GetCharID( m_dwCharID );
	if ( pChar )
	{
		pChar->SETITEMSHOP( m_vItem );
		pChar->SENDBOXITEMSHOPINFO();
	}
	
    return NET_OK;
}
CSetItemShop::CSetItemShop(
	CString strItemNum,
	DWORD dwUserID )
{
    m_strItemNum = strItemNum;
    m_dwUserID = dwUserID;
}

int CSetItemShop::Execute(
	CServer* pServer )
{
    return m_pDbManager->SetItemShop( m_strItemNum, m_dwUserID );
}

CItemShop_Get::CItemShop_Get(
	DWORD dwClient,
	DWORD dwUserID,
	CString strPurKey )
	:  CDbAction( dwClient )
{	
	m_dwUserID  = dwUserID;
	m_dwClient  = dwClient;
	m_strPurKey = strPurKey;
}

int CItemShop_Get::Execute(
	CServer* pServer )
{
	GLMSG::SNET_ITEMSHOP_ITEM_BUY NetMsg;
	int nRET = m_pDbManager->SetItemShop( m_strPurKey, m_dwUserID );
	if ( nRET == 1 )
	{
		NetMsg.dwUserID = m_dwUserID;
		NetMsg.bBuy = true;
	}else NetMsg.bBuy = false;
	CFieldServer* pTemp = reinterpret_cast<CFieldServer*> (pServer);
	if ( pTemp ) 
	{
		pTemp->InsertMsg ( m_dwClient, (char*) &NetMsg );
	}
	return nRET;
}