#pragma once
#include "GLContrlBaseMsg.h"
#define RANKING_PKNUM		30
namespace GLMSG
{
	#pragma pack(1)
	
	//dmk14 | 11-4-16 | pk ranking
	struct SNET_MSG_REQ_PLAYERRANKING
	{
		NET_MSG_GENERIC		nmg;
		DWORD				dwMapID;		
		
		SNET_MSG_REQ_PLAYERRANKING()
			: dwMapID(UINT_MAX)
		{
			nmg.dwSize = sizeof(SNET_MSG_REQ_PLAYERRANKING);
			nmg.nType = NET_MSG_GCTRL_REQ_PLAYERRANKING;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_MSG_REQ_PLAYERRANKING_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;
		PLAYER_RANKING		sPlayerRank[RANKING_PKNUM];

		SNET_MSG_REQ_PLAYERRANKING_UPDATE()
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_REQ_PLAYERRANKING_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDCHAR ( const PLAYER_RANKING& sRank )
		{
			if ( RANKING_PKNUM==wRankNum )		return false;

			sPlayerRank[wRankNum] = sRank;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(PLAYER_RANKING)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}
		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_MSG_REQ_PLAYERRANKING_UPDATE_EX
	{
		NET_MSG_GENERIC			nmg;
		
		PLAYER_RANKING_EX		sMyPKRank;		

		SNET_MSG_REQ_PLAYERRANKING_UPDATE_EX () 
		{
			nmg.dwSize = sizeof(SNET_MSG_REQ_PLAYERRANKING_UPDATE_EX);
			nmg.nType = NET_MSG_GCTRL_REQ_PLAYERRANKING_UPDATE_EX;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	//dmk14 | 11-28-16 | cw scoring
	/*struct SNET_MSG_REQ_GUILDRANKING
	{
		NET_MSG_GENERIC		nmg;
		bool				bCP;
		
		SNET_MSG_REQ_GUILDRANKING()
			: bCP( false )
		{
			nmg.dwSize = sizeof(SNET_MSG_REQ_GUILDRANKING);
			nmg.nType = NET_MSG_GCTRL_REQ_GUILDRANKING;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_MSG_REQ_GUILDRANKING_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;
		GUILD_RANKING		sGuildRank[5];

		SNET_MSG_REQ_GUILDRANKING_UPDATE()
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_REQ_GUILDRANKING_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDGUILD ( const GUILD_RANKING& sRank )
		{
			if ( 5==wRankNum )		return false;

			sGuildRank[wRankNum] = sRank;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(GUILD_RANKING)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}
		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};

	struct SNET_MSG_REQ_GUILDRANKING_UPDATE_EX
	{
		NET_MSG_GENERIC			nmg;
		
		GUILD_RANKING_EX		sMyGuildRank;		

		SNET_MSG_REQ_GUILDRANKING_UPDATE_EX () 
		{
			nmg.dwSize = sizeof(SNET_MSG_REQ_GUILDRANKING_UPDATE_EX);
			nmg.nType = NET_MSG_GCTRL_REQ_GUILDRANKING_UPDATE_EX;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);			
		}        
	};

	struct SNET_MSG_REQ_SCHOOLRANKING
	{
		NET_MSG_GENERIC		nmg;
		
		SNET_MSG_REQ_SCHOOLRANKING()
		{
			nmg.dwSize = sizeof(SNET_MSG_REQ_SCHOOLRANKING);
			nmg.nType = NET_MSG_GCTRL_REQ_SCHOOLRANKING;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}
	};

	struct SNET_MSG_REQ_SCHOOLRANKING_UPDATE
	{
		NET_MSG_GENERIC		nmg;
		WORD				wRankNum;
		SCHOOL_RANKING		sSchoolRank[3];

		SNET_MSG_REQ_SCHOOLRANKING_UPDATE()
			: wRankNum(0)
		{
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
			nmg.nType = NET_MSG_GCTRL_REQ_SCHOOLRANKING_UPDATE;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);
		}

		bool ADDSCHOOL ( const SCHOOL_RANKING& sRank )
		{
			if ( 3==wRankNum )		return false;

			sSchoolRank[wRankNum] = sRank;

			++wRankNum;

			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD) + sizeof(SCHOOL_RANKING)*wRankNum;
			GASSERT(nmg.dwSize<=NET_DATA_BUFSIZE);

			return true;
		}
		void RESET ()
		{
			wRankNum = 0;
			nmg.dwSize = sizeof(NET_MSG_GENERIC) + sizeof(WORD);
		}
	};*/

	// Revert to default structure packing
	#pragma pack()
};