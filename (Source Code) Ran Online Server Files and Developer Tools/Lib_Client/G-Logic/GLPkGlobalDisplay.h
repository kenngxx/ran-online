
#include "GLCharDefine.h"
#include <set>

#pragma once

#define PK_DISPLAY_NUM		10 
#define PK_DISPLAY_NAME		33

struct SPK_GLOBAL_DISPAY
{
	DWORD	dwCharIDKill;
	BYTE	nSCHOOLKill;
	BYTE	nCLASSKill;

	DWORD	dwCharIDKilled;
	BYTE	nSCHOOLKilled;
	BYTE	nCLASSKilled;

	char	szCharNameKill[PK_DISPLAY_NAME+1];	
	char	szCharNameKilled[PK_DISPLAY_NAME+1];	

	SPK_GLOBAL_DISPAY()
		: dwCharIDKill( 0 )
		, nSCHOOLKill(0)
		, nCLASSKill(0)
		, dwCharIDKilled( 0 )
		, nSCHOOLKilled(0)
		, nCLASSKilled(0)
	{
		memset( szCharNameKill, 0, sizeof(char) * (PK_DISPLAY_NAME+1) );
		memset( szCharNameKilled, 0, sizeof(char) * (PK_DISPLAY_NAME+1) );
	}

};

struct SPK_GLOBAL_DISPAY_CLIENT
{  
	DWORD	dwCharIDKill;
	BYTE	nSCHOOLKill;
	BYTE	nCLASSKill;

	DWORD	dwCharIDKilled;
	BYTE	nSCHOOLKilled;
	BYTE	nCLASSKilled;

	char	szCharNameKill[PK_DISPLAY_NAME+1];	
	char	szCharNameKilled[PK_DISPLAY_NAME+1];	

	SPK_GLOBAL_DISPAY_CLIENT()
		: dwCharIDKill( 0 )
		, nSCHOOLKill(0)
		, nCLASSKill(0)
		, dwCharIDKilled( 0 )
		, nSCHOOLKilled(0)
		, nCLASSKilled(0)
	{
		memset( szCharNameKill, 0, sizeof(char) * (PK_DISPLAY_NAME+1) );
		memset( szCharNameKilled, 0, sizeof(char) * (PK_DISPLAY_NAME+1) );
	}

};


