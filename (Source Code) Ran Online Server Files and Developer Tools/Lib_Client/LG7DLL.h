/////////////////////////////////////////////////////////////////////////////
//	FileName	: LG7DLL.h
//	Project		: RanClientLib
//	Coder		: LG-7

#pragma once

#include "../Lib_Engine/GUInterface/UIDataType.h"
#include "../Lib_Network/s_NetClient.h"
#include "../Lib_Client/G-Logic/GLItem.h"

#include <vector>

/////////////////////////////////////////////////////////////////////////////
// LG-7 GlobalRanking
#define GLOBAL_RANKING_SIZE	12
#define MAX_TOP_RANK		50
#define MAX_ONE_VIEW_RANK	10

struct STOP_RANK_KILL
{
	DWORD	dwChaNum;
	char	szChaName[33];
	WORD	wChaLevel;
	int		nChaClass;
	WORD	wChaSchool;
	BOOL	bChaOnline;
	WORD	wGuNum;
	WORD	wGuMarkVer;
	char	szGuName[33];
	DWORD	dwChaPKWin;
	DWORD	dwChaPKLoss;

	STOP_RANK_KILL()
		: dwChaNum(0)
		, wChaLevel(0)
		, nChaClass(0)
		, wChaSchool(0)
		, bChaOnline(FALSE)
		, wGuNum(0)
		, wGuMarkVer(0)
		, dwChaPKWin(0)
		, dwChaPKLoss(0)
	{
		SecureZeroMemory(szChaName,	sizeof(char) * 33);
		SecureZeroMemory(szGuName,	sizeof(char) * 33);
	}
};

struct STOP_RANK_RICH
{
	DWORD		dwChaNum;
	char		szChaName[33];
	WORD		wChaLevel;
	int			nChaClass;
	WORD		wChaSchool;
	BOOL		bChaOnline;
	WORD		wGuNum;
	WORD		wGuMarkVer;
	char		szGuName[33];
	LONGLONG	llnMoney;

	STOP_RANK_RICH()
		: dwChaNum(0)
		, wChaLevel(0)
		, nChaClass(0)
		, wChaSchool(0)
		, bChaOnline(FALSE)
		, wGuNum(0)
		, wGuMarkVer(0)
		, llnMoney(0)
	{
		SecureZeroMemory(szChaName,	sizeof(char) * 33);
		SecureZeroMemory(szGuName,	sizeof(char) * 33);
	}
};

struct STOP_RANK_GUILD
{
	WORD	wGuNum;
	char	szGuName[33];
	WORD	wRank;
	WORD	wGuMarkVer;
	WORD	wAlliance;
	WORD	wOnline;
	DWORD	dwWin;
	DWORD	dwLoss;
	DWORD	dwDraw;

	STOP_RANK_GUILD()
		: wGuNum(0)
		, wRank(0)
		, wGuMarkVer(0)
		, wAlliance(0)
		, wOnline(0)
		, dwWin(0)
		, dwLoss(0)
		, dwDraw(0)
	{
		SecureZeroMemory(szGuName, sizeof(char) * 33);
	}
};

typedef std::vector<STOP_RANK_KILL>		VEC_TOP_KILL;
typedef VEC_TOP_KILL::iterator			VEC_TOP_KILL_ITER;

typedef std::vector<STOP_RANK_RICH>		VEC_TOP_RICH;
typedef VEC_TOP_RICH::iterator			VEC_TOP_RICH_ITER;

typedef std::vector<STOP_RANK_GUILD>	VEC_TOP_GUILD;
typedef VEC_TOP_GUILD::iterator			VEC_TOP_GUILD_ITER;
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// LG-7 CharView
struct SCHAR_VIEW
{
	SITEMCUSTOM	m_PutOnItems_[SLOT_NSIZE_S_2];
	WORD		wChaHair;
	WORD		wChaHairColor;
	WORD		wChaFace;
	WORD		wChaLevel;
	int			nChaClass;
	WORD		wChaSchool;

	WORD		wGuNum;
	WORD		wGuMarkVer;

	char		szChaName[33];
	char		szGuName[33];

	int			nAttLow;
	int			nAttHigh;
	int			nDef;
	WORD		wMelee;
	WORD		wRange;
	WORD		wMagic;
	DWORD		dwMaxHP;
	int			nMaxMP;
	int			nMaxSP;
	int			nAcc;
	int			nEva;

	SCHAR_VIEW()
		: wChaHair(-1)
		, wChaHairColor(-1)
		, wChaFace(-1)
		, wChaLevel(-1)
		, nChaClass(0)
		, wChaSchool(0)

		, wGuNum(0)
		, wGuMarkVer(0)

		, nAttLow(-1)
		, nAttHigh(-1)
		, nDef(-1)
		, wMelee(-1)
		, wRange(-1)
		, wMagic(-1)
		, dwMaxHP(-1)
		, nMaxMP(-1)
		, nMaxSP(-1)
		, nAcc(-1)
		, nEva(-1)
	{
		SecureZeroMemory(szChaName,	sizeof(char) * 33);
		SecureZeroMemory(szGuName,	sizeof(char) * 33);
	}
};
/////////////////////////////////////////////////////////////////////////////

class CLG7_DLL
{
public:
	virtual HRESULT	OneTimeSceneInitClient()	= 0;
	virtual HRESULT	OneTimeSceneInitServer()	= 0;

	// LG-7 CharView
	virtual UIRECT		CCharViewWindow_UIRECT_RcTHIS()					= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcLBoxA_(int nIndex)		= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcText_(int nIndex)		= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcItemImage_(int nIndex)	= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcClassImage()			= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcSchoolImage()			= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcGuildImage()			= 0;
	virtual UIRECT		CCharViewWindow_UIRECT_RcRender()				= 0;
	/////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////
	// LG-7 GlobalRanking
	virtual UIRECT		CGlobalRankingWindow_UIRECT_RcButtonTab_(int nIndex)							= 0;
	virtual UIRECT		CGlobalRankingWindow_UIRECT_RcWhiteBG()											= 0;
	virtual UIRECT		CGlobalRankingWindow_UIRECT_RcTHIS()											= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcLBox(int nIndex)									= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcLBoxA_(int nIndex1, int nIndex2, int nIndex3)		= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcText_(int nIndex1, int nIndex2)						= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcTHIS(int nIndex)									= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcScrollBar(int nIndex)								= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcSlotDummy_(int nIndex1, int nIndex2)				= 0;
	virtual UIRECT		CGlobalRankingPage_UIRECT_RcSlot_(int nIndex1, int nIndex2)						= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcLBoxA_(int nIndex1, int nIndex2, int nIndex3)	= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcDummy(int nIndex1, int nIndex2)					= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcText_(int nIndex1, int nIndex2, int nIndex3)	= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcClassImage(int nIndex1, int nIndex2)			= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcSchoolImage(int nIndex1, int nIndex2)			= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcGuildImage(int nIndex1, int nIndex2)			= 0;
	virtual UIRECT		CGlobalRankingPageSlot_UIRECT_RcTHIS(int nIndex1, int nIndex2)					= 0;

	virtual VEC_TOP_KILL&	GetTopKill(int nIndex)	= 0;
	virtual VEC_TOP_RICH&	GetTopRich()			= 0;
	virtual VEC_TOP_GUILD&	GetTopGuild()			= 0;

	virtual HRESULT			CleanTime()				= 0;
	/////////////////////////////////////////////////////////////////////////////


	/////////////////////////////////////////////////////////////////////////////
	// LG-7 AntiFlood
	virtual BOOL		SetAntiFlood(DWORD dwMax, DWORD dwCur)	= 0;
	virtual BOOL		IsAntiFloodContinue()					= 0;
	virtual DWORD		GetClientCheckTime()					= 0;
	virtual BOOL		IsClientCHECK()							= 0;
	virtual BOOL		SetClientCHECK(BOOL bCheck)				= 0;
	virtual BOOL		SetClientCheckTime(DWORD dwTime)		= 0;
	/////////////////////////////////////////////////////////////////////////////
};