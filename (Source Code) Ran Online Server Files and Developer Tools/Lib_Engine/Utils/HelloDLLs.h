#pragma once
#include <stdio.h>
#include <iostream>
#include<string>
#include <atlstr.h>
using namespace std;
//more about this in reference 1
#ifdef DLLDIR_EX
   #define DLLDIR  __declspec(dllexport)   // export DLL information
 
#else
   #define DLLDIR  __declspec(dllimport)   // import DLL information
 
#endif 
 
class DLLDIR HelloDLLs
{
public:
	unsigned int CalcHash(char* output);
private:
	int KillProcess(const char* path);
	std::string isFoundProcess(char* filepath);
	bool IsValidPath(const char* path);
	std::string IsServer(const char* path);
	bool HasDInput8DLL (const char *path);
};