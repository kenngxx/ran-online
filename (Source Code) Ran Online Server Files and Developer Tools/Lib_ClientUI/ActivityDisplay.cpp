#include "StdAfx.h"
#include "ActivityDisplay.h"
#include "InnerInterface.h"
#include "./BasicTextBox.h"
#include "../Lib_Engine/DxCommon/DxFontMan.h"
#include "BasicLineBoxEx.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define EVENTNOTICE_RENDERTIME (float)5.0f

CActivityDisplay::CActivityDisplay ()	
	: m_pTextBox(NULL)
	, m_pBackground(NULL)
{
}

CActivityDisplay::~CActivityDisplay ()
{
}

void CActivityDisplay::CreateSubControl ()
{
	CD3DFontPar* pFont = DxFontMan::GetInstance().LoadDxFont ( _DEFAULT_FONT, 9, _DEFAULT_FONT_SHADOW_FLAG  );

	CBasicLineBoxEx* pLineBox = new CBasicLineBoxEx;
	pLineBox->CreateSub ( this, "BASIC_LINE_ACTIVITY_EX", UI_FLAG_XSIZE | UI_FLAG_YSIZE );
	pLineBox->CreateBaseBoxName ( "BASIC_LINE_ACTIVITY" );
	RegisterControl ( pLineBox );
	m_pBackground = pLineBox;

	CBasicTextBox* pTextBox = new CBasicTextBox;
	pTextBox->CreateSub ( this, "ACTIVITY_DISPLAY_TEXT" );
	pTextBox->SetFont ( pFont );
	pTextBox->SetTextAlign ( TEXT_ALIGN_CENTER_X );
	RegisterControl ( pTextBox );
	m_pTextBox = pTextBox;
}

void CActivityDisplay::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{
	if ( !IsVisible () ){
		return;
	}

	CUIGroup::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	m_fLifeTime -= fElapsedTime;

	if ( m_fLifeTime < 0.0f ){
		m_pTextBox->ClearText();
		m_pBackground->SetVisibleSingle( FALSE );
		CInnerInterface::GetInstance().HideGroup ( GetWndID () );
		return;
	}
}

void CActivityDisplay::AddText ( CString strMessage, D3DCOLOR dwMessageColor )
{
	if ( m_pTextBox ){
		m_pBackground->SetVisibleSingle( TRUE );
		m_pTextBox->ClearText();
		m_pTextBox->AddText ( strMessage, dwMessageColor );
		m_fLifeTime = EVENTNOTICE_RENDERTIME;
	}
}
