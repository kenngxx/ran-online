#pragma	once

#include "UIOuterWindow.h"
#include "UIWindowEx.h"

class CUIEditBoxMan;

class CPersonalLockerWindow : public CUIWindowEx
{
	enum
	{
		RESETPASS_OK = ET_CONTROL_NEXT,
		RESETPASS_CANCEL,
		RESETPASS_EDIT_MAN,
		RESETPASS_EDIT_ID,
		RESETPASS_EDIT_PW2,
		RESETPASS_EDIT_CP,
		/*equipment lock, Juver, 2018/01/16 */
		BUTTON_KEY_EQUIPMENT_LOCK,
		BUTTON_KEY_EQUIPMENT_UNLOCK,
		/*inventory lock, Nell, 2020/01/16 */
		BUTTON_KEY_INVENTORY_LOCK,
		BUTTON_KEY_INVENTORY_UNLOCK,
		/*storage lock, Nell, 2020/01/16 */
		BUTTON_KEY_STORAGE_LOCK,
		BUTTON_KEY_STORAGE_UNLOCK,

	/*allin lock, Nell, 2020/01/16 */
		BUTTON_KEY_EQUIPMENT_ALL_LOCK,
		BUTTON_KEY_INVENTORY_ALL_LOCK,
		BUTTON_KEY_STORAGE_ALL_LOCK
	};

public:
	static int nLIMIT_ID;
	static int nLIMIT_PW2;
	static int nLIMIT_CP;

public:
	CPersonalLockerWindow();
	virtual	~CPersonalLockerWindow();

public:
	virtual void Update( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage( UIGUID ControlID, DWORD dwMsg );

	//virtual	void SetVisibleSingle( BOOL bVisible );

public:
	void CreateSubControl();
	CBasicButton * CreateFlipButton( char* szButton, char* szButtonFlip, UIGUID ControlID );

	void ResetAll();
	void SetCharToEditBox( TCHAR cKey );
	void DelCharToEditBox();
	void GoNextTab();

private:
	//BOOL CheckString( CString strTemp );
private:
	/*equipment lock, Juver, 2018/01/16 */
	CBasicButton*	m_pButtonEquipmentLock;
	CBasicButton*	m_pButtonEquipmentUnlock;
	/*inventory lock, Nell, 2020/01/16 */
	CBasicButton*	m_pButtonInventoryLock;
	CBasicButton*	m_pButtonInventoryUnlock;
	/*storage lock, Nell, 2020/01/16 */
	CBasicButton*	m_pButtonStorageLock;
	CBasicButton*	m_pButtonStorageUnlock;


		/*allin lock, Nell, 2020/01/16 */
	CBasicButton*	m_pButtonEquipmentAllinLock;
	CBasicButton*	m_pButtonInventoryAllinLock;
	CBasicButton*	m_pButtonStorageAllinLock;
	CUIEditBoxMan * m_pEditBoxMan;
	CBasicButton * m_pIDSaveButton;
	CBasicTextBox * m_pRandTextBox;

	INT m_nRandPassNumber;
	TCHAR m_szRandomString[7];
	INT m_nRPUpdateCnt;
};