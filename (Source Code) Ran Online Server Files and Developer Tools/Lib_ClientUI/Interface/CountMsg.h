#pragma	once

#include "../Lib_Engine/GUInterface/UIGroup.h"

class	CBasicTextBox;
class	CBasicTextBoxEx;
class	CBasicLineBoxEx;
class	CD3DFontPar;

class CCountMsg : public CUIGroup
{
public:
	CCountMsg ();
	virtual	~CCountMsg ();

public:
	void CreateSubControl();
	CUIControl*	CreateControl( CONST TCHAR* szControl );
	void SetCount( INT nCount );
	void SetText( INT nNum );
	void SetTextWin( INT nNum );
	
	CBasicTextBox*		m_pTextBoxNum; 

public:
	virtual void Update( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CUIControl * m_pTEN[10];
	CUIControl * m_pONE[10];
	CD3DFontPar*		m_pFont;
	CD3DFontPar*		m_pFont20;
	CBasicLineBoxEx*	m_pLineBoxGray;
	bool CanCount;
	bool SetCount() { return CanCount=true; }
	bool ReSetCount() { return  CanCount=false; }
	int nCount;
	INT m_nCount;
	float m_fElapsedTime;
	float fCountdown;
};