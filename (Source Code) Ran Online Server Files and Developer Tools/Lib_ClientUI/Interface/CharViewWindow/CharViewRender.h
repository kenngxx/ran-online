/////////////////////////////////////////////////////////////////////////////
//	FileName	: CharViewRender.h
//	Project		: Lib -- RanClientUI
//	Coder		: LG-7

#pragma once

#include "../Lib_Engine/Meshs/DxSkinChar.h"
#include "../Lib_Engine/GUInterface/UIGroup.h"
#include "../Lib_Client/G-Logic/GLGaeaClient.h"

class CCharViewRender : public CUIGroup
{
private:
	BOOL					m_bReady;
	DxSkinChar* m_pSkinChar;
	D3DXVECTOR3				m_vRot;
	D3DXMATRIX				m_matTrans;

	SCHAR_VIEW				m_sCharView;

public:
	CCharViewRender();
	virtual ~CCharViewRender();

public:
	void CreateSubControl();
	virtual HRESULT RestoreDeviceObjects(LPDIRECT3DDEVICEQ pd3dDevice);
	virtual HRESULT DeleteDeviceObjects();
	virtual HRESULT Render(LPDIRECT3DDEVICEQ pd3dDevice);
	virtual HRESULT FrameMove(LPDIRECT3DDEVICEQ pd3dDevice, float fTime, float fElapsedTime);

private:
	void CreateModel(LPDIRECT3DDEVICEQ pd3dDevice);
	void RenderModel(LPDIRECT3DDEVICEQ pd3dDevice);
	void UpdateSuit(LPDIRECT3DDEVICEQ pd3dDevice);

public:
	void RESET();
	void StartRender(SCHAR_VIEW sCharView);
};