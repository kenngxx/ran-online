#pragma	once

#include "../Lib_Client/G-Logic/GLogicEx.h"
#include "UIWindowEx.h"
#include "GLCharDefine.h"

class	CD3DFontPar;
class	CBasicProgressBar;
class	CBasicTextBox;
class	CBasicButtonText;
class	CBasicButton;
class	CCharacterWindowCharStatMark;
struct	GLCHARLOGIC;