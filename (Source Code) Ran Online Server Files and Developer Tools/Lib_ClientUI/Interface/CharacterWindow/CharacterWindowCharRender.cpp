#include "StdAfx.h"
#include "CharacterWindowCharRender.h"

#include "../DxCommon/DxBackUpRendTarget.h"

#include "GLItemMan.h"
#include "GLogicData.h"
#include "DxEffcharData.h"
#include "RANPARAM.h"
#include "GLGaeaClient.h"
#include "DxGlobalStage.h"

#include "DxEffectMan.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif