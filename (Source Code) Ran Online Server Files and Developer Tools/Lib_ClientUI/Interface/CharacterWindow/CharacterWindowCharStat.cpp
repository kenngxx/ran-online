#include "StdAfx.h"
#include "../Lib_Client/G-Logic/GLGaeaClient.h"
#include "../Lib_Engine/DxCommon/d3dfont.h"

#include "BasicTextBox.h"
#include "BasicProgressBar.h"
#include "BasicButtonText.h"
#include "BasicButton.h"

#include "BasicTextButton.h"
#include "UITextControl.h"
#include "GameTextControl.h"
#include "InnerInterface.h"
#include "ModalCallerID.h"
#include "ModalWindow.h"
#include "BasicLineBox.h"
#include "GLItemMan.h"
#include "GLItem.h"

#include "CharacterWindowCharStat.h"
#include "CharacterWindowCharStatMark.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif