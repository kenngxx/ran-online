//	마켓 윈도우
//
//	최초 작성자 : 성기엽
//	이후 수정자 : 
//	로그
//		[2003.12.6]
//			@ 작성
//

#pragma	once

#include "../Lib_Engine/GUInterface/UIGroup.h"
#include "GLInventory.h"

class	CItemSlotEx;
class	CBasicButton;
class	CBasicTextBox;
class	CD3DFontPar;
class	CBasicScrollBarEx;
struct	SINVENITEM;

class	CTradeInventoryPage : public CUIGroup
{
private:
static	const	int	nSTARTLINE;
static	const	int	nOUTOFRANGE;

protected:
	enum
	{
		nONE_VIEW_SLOT_DEFAULT = 10,
		nMAX_ONE_VIEW_SLOT = 10,
		nMAX_ONE_VIEW_SLOT2 = 20,
		nMAX_ONE_VIEW_SLOT3 = 30,
		nMAX_ONE_VIEW_SLOT4 = 40,
		nMAX_ONE_VIEW_SLOT5 = 50
	};

protected:
	enum
	{
		ITEM_SLOT0 = NO_ID + 1,
		ITEM_SLOT1,
		ITEM_SLOT_END = ITEM_SLOT0 + EM_INVENSIZE_Y,
		TRADEINVENTORY_BUTTON0,
		TRADEINVENTORY_BUTTON1,
		TRADEINVENTORY_BUTTON2,
		TRADEINVENTORY_BUTTON3,
		TRADEINVENTORY_BUTTON4,
	};

public:
	CTradeInventoryPage ();
	virtual	~CTradeInventoryPage ();

public:
	void	CreateSubControl ();

	void	UpdateBLOCK ();

	void	TRADEINVENTORY1();
	void	TRADEINVENTORY2();
	void	TRADEINVENTORY3();
	void	TRADEINVENTORY4();
	void	TRADEINVENTORY5();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );

protected:
	void	ResetAllItemSlotRender ( int index,int nTotal );
	void	SetItemSlotRender ( int nStartIndex, int nTotal );

protected:
	CItemSlotEx*	CreateItemSlot ( CString strKeyword, UIGUID ControlID );
	CUIControl*	CreateControl ( const char* szControl );

	CUIControl*		m_Button[5];
	CUIControl*		m_ButtonSet[5];

public:
	void	SetOneViewSlot ( const int& nMAX_ONE_VIEW );
	const int& GetOneViewSlot () const						{ return m_nONE_VIEW_SLOT; }

public:
	void	SetItemIndex ( int nPosX, int nPosY );
	void	GetItemIndex ( int* pnPosX, int* pnPosY );

public:
	void	LoadItemPage ( GLInventory &ItemData );
	void	LoadItem ( SINVENITEM& ref_InvenItem );

	SINVENITEM&	GetItem ( int nPosX, int nPosY );

public:
	void	UnLoadItemPage ();
	void	UnLoadItem ( int nPosX, int nPosY );

public:
	void	ResetAllFlipItem ();
	void	UpdateFlipItem ( GLInventory& ref_TradeInventory );

private:
	CItemSlotEx*	m_pItemSlotArray[EM_INVENSIZE_Y];
	CUIControl*		m_pItemSlotArrayDummy[nMAX_ONE_VIEW_SLOT5];
	CBasicButton*	m_pPageButton[5];
//	CBasicScrollBarEx*	m_pScrollBar;

private:
	int		m_nCurPos;
	int		m_nPosX;
	int		m_nPosY;

private:
	int		m_nONE_VIEW_SLOT;
};