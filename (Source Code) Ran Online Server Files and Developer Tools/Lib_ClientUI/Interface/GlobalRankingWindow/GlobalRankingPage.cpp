/////////////////////////////////////////////////////////////////////////////
//	FileName	: GlobalRankingPage.cpp
//	Project		: RanClientUILib
//	Coder		: LG-7

#include "stdafx.h"
#include "GlobalRankingPage.h"
#include "GlobalRankingPageSlot.h"

#include "../BasicLineBox.h"
#include "../BasicScrollBarEx.h"
#include "../GameTextControl.h"

#include "../lib_engine/DxCommon/DxFontMan.h"
#include "./BasicTextBox.h"
#include "../Lib_ClientUI/Interface/BasicScrollThumbFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const int CGlobalRankingPage::nSTARTLINE = 0;

CGlobalRankingPage::CGlobalRankingPage()
	: m_nIndex(-1)
	, m_nSize(-1)
	, m_nState(0)
	, m_nCurPos(-1)

	, m_pLBox(NULL)
	, m_pScrollBar(NULL)
{
	memset(m_pLBoxA_,		NULL, sizeof(m_pLBoxA_));
	memset(m_pText_,		NULL, sizeof(m_pText_));
	memset(m_pSlotDummy_,	NULL, sizeof(m_pSlotDummy_));
	memset(m_pSlot_,		NULL, sizeof(m_pSlot_));
}

CGlobalRankingPage::~CGlobalRankingPage()
{
}

void CGlobalRankingPage::CreateSubControl(int nIndex)
{
	m_nIndex = nIndex;

	switch (m_nIndex)
	{
	case 2:		m_nSize = 7; break;
	default:	m_nSize = 8; break;
	}

	m_pLBox = new CBasicLineBox;
	m_pLBox->CreateSub(this, "BASIC_LINE_BOX_BODY_OUTER", UI_FLAG_XSIZE | UI_FLAG_YSIZE);
	m_pLBox->CreateBaseBoxOuter("BASIC_LINE_BOX_BODY_OUTER");
	RegisterControl(m_pLBox);

	CD3DFontPar* pFont8 = DxFontMan::GetInstance().LoadDxFont(_DEFAULT_FONT, 8, _DEFAULT_FONT_SHADOW_FLAG);
	const int nAlignMid = TEXT_ALIGN_CENTER_X | TEXT_ALIGN_CENTER_Y;

	for (int i = 0; i < m_nSize; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			m_pLBoxA_[i][j] = new CBasicLineBox;
			m_pLBoxA_[i][j]->CreateSub(this, "BASIC_LINE_BOX_BODY_OUTER_BLANK_WHITE", UI_FLAG_XSIZE | UI_FLAG_YSIZE);
			m_pLBoxA_[i][j]->CreateBaseBoxOuterBlankWhite("BASIC_LINE_BOX_BODY_OUTER_BLANK_WHITE");
			RegisterControl(m_pLBoxA_[i][j]);
		}

		m_pText_[i] = CreateStaticControl("BASIC_WINDOW", pFont8, nAlignMid);
	}

	m_pScrollBar = new CBasicScrollBarEx;
	m_pScrollBar->CreateSub(this, "BASIC_SCROLLBAR", UI_FLAG_RIGHT | UI_FLAG_YSIZE, SCROLLBAR);
	m_pScrollBar->CreateBaseScrollBar("GLOBAL_RANKING_WINDOW_SCROLLBAR");
	m_pScrollBar->GetThumbFrame()->SetState(m_nState, MAX_ONE_VIEW_RANK);
	RegisterControl(m_pScrollBar);

	for (int i = 0; i < MAX_ONE_VIEW_RANK; i++)
	{
		m_pSlotDummy_[i] = new CUIControl;
		m_pSlotDummy_[i]->CreateSub(this, "BASIC_WINDOW", UI_FLAG_XSIZE | UI_FLAG_YSIZE);
		RegisterControl(m_pSlotDummy_[i]);
	}

	for (int i = 0; i < MAX_TOP_RANK; i++)
	{
		m_pSlot_[i] = new CGlobalRankingPageSlot;
		m_pSlot_[i]->CreateSub(this, "BASIC_WINDOW", UI_FLAG_XSIZE | UI_FLAG_YSIZE);

		if(m_nIndex == 2)	m_pSlot_[i]->CreateSubControl(m_nIndex, i, TRUE);
		else				m_pSlot_[i]->CreateSubControl(m_nIndex, i, FALSE);

		m_pSlot_[i]->SetVisibleSingle(FALSE);
		RegisterControl(m_pSlot_[i]);
	}

	DefaultPos();
	SetSlotRender(nSTARTLINE, MAX_ONE_VIEW_RANK);
}

void CGlobalRankingPage::Update(int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl)
{
	CUIGroup::Update(x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl);

	if (m_pScrollBar)
	{
		CBasicScrollThumbFrame* pThumbFrame = m_pScrollBar->GetThumbFrame();
		int nTotal = pThumbFrame->GetTotal();

		if (nTotal <= MAX_ONE_VIEW_RANK)
			return;

		const int nViewPerPage = pThumbFrame->GetViewPerPage();

		if (nViewPerPage < nTotal)
		{
			int nCurPos = nSTARTLINE;
			const int nMovableLine = nTotal - nViewPerPage;
			float fPercent = pThumbFrame->GetPercent();
			nCurPos = (int)floor(fPercent * nMovableLine);

			if (nCurPos < nSTARTLINE)
				nCurPos = nSTARTLINE;

			if (m_nCurPos == nCurPos)
				return;

			m_nCurPos = nCurPos;

			ResetAllSlotRender(nTotal);
			SetSlotRender(nCurPos, nCurPos + nViewPerPage);
		}
	}
}

CBasicTextBox* CGlobalRankingPage::CreateStaticControl(char* szControlKeyword, CD3DFontPar* pFont, int nAlign, const UIGUID& cID)
{
	CBasicTextBox* pStaticText = new CBasicTextBox;
	pStaticText->CreateSub(this, szControlKeyword, UI_FLAG_DEFAULT, cID);
	pStaticText->SetFont(pFont);
	pStaticText->SetTextAlign(nAlign);
	RegisterControl(pStaticText);

	return pStaticText;
}

void CGlobalRankingPage::DefaultPos()
{
	UIRECT RcOLD;
	UIRECT RcNEW1;

	// m_pLBox
	{
		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcLBox(m_nIndex);
		/////////////////////////////////////////////////////////////////////////////

		RcOLD = m_pLBox->GetLocalPos();
		m_pLBox->AlignSubControl(RcOLD, RcNEW1);
		m_pLBox->SetLocalPos(RcNEW1);
		m_pLBox->SetGlobalPos(RcNEW1);
	}

	// m_pLBoxA_ | m_pText_
	for (int i = 0; i < m_nSize; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			/////////////////////////////////////////////////////////////////////////////
			// LG-7 DLLCaller
			RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcLBoxA_(m_nIndex, i, j);
			/////////////////////////////////////////////////////////////////////////////

			RcOLD = m_pLBoxA_[i][j]->GetLocalPos();
			m_pLBoxA_[i][j]->AlignSubControl(RcOLD, RcNEW1);
			m_pLBoxA_[i][j]->SetLocalPos(RcNEW1);
			m_pLBoxA_[i][j]->SetGlobalPos(RcNEW1);
		}

		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcText_(m_nIndex, i);
		/////////////////////////////////////////////////////////////////////////////

		RcOLD = m_pText_[i]->GetLocalPos();
		m_pText_[i]->AlignSubControl(RcOLD, RcNEW1);
		m_pText_[i]->SetLocalPos(RcNEW1);
		m_pText_[i]->SetGlobalPos(RcNEW1);

		switch (m_nIndex)
		{
		case 1:		m_pText_[i]->SetOneLineText((char*)ID2GAMEWORD("GLOBAL_RANKING_PAGE_TEXT", 10 + i));	break;
		case 2:		m_pText_[i]->SetOneLineText((char*)ID2GAMEWORD("GLOBAL_RANKING_PAGE_TEXT", 20 + i));	break;
		default:	m_pText_[i]->SetOneLineText((char*)ID2GAMEWORD("GLOBAL_RANKING_PAGE_TEXT", i));			break;
		}	
	}

	// m_pScrollBar
	{
		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcScrollBar(m_nIndex);
		/////////////////////////////////////////////////////////////////////////////

		RcOLD = m_pScrollBar->GetLocalPos();
		m_pScrollBar->AlignSubControl(RcOLD, RcNEW1);
		m_pScrollBar->SetLocalPos(RcNEW1);
		m_pScrollBar->SetGlobalPos(RcNEW1);
	}

	// m_pSlotDummy_ 
	for (int i = 0; i < MAX_ONE_VIEW_RANK; i++)
	{
		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcSlotDummy_(m_nIndex, i);
		/////////////////////////////////////////////////////////////////////////////

		RcOLD = m_pSlotDummy_[i]->GetLocalPos();
		m_pSlotDummy_[i]->AlignSubControl(RcOLD, RcNEW1);
		m_pSlotDummy_[i]->SetLocalPos(RcNEW1);
	}

	// m_pSlot_
	for (int i = 0; i < MAX_TOP_RANK; i++)
	{
		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcSlot_(m_nIndex, i);
		/////////////////////////////////////////////////////////////////////////////

		RcOLD = m_pSlot_[i]->GetLocalPos();
		m_pSlot_[i]->AlignSubControl(RcOLD, RcNEW1);
		m_pSlot_[i]->SetLocalPos(RcNEW1);
	}

	{
		/////////////////////////////////////////////////////////////////////////////
		// LG-7 DLLCaller
		RcNEW1 = DxGlobalStage::GetInstance().GetLG7()->CGlobalRankingPage_UIRECT_RcTHIS(m_nIndex);
		/////////////////////////////////////////////////////////////////////////////

		SetLocalPos(RcNEW1);
	}
}

void CGlobalRankingPage::SetSlotRender(int nStartIndex, int nTotal)
{
	if (nTotal < 0) return;

	const UIRECT& rcParentPos = GetGlobalPos();

	for (int i = nStartIndex; i < nTotal; i++)
	{
		CGlobalRankingPageSlot* pSlot = m_pSlot_[i];
		if (pSlot)
		{
			int nAbsoluteIndex				= i - nStartIndex;
			CUIControl* pDummyControl		= m_pSlotDummy_[nAbsoluteIndex];
			const UIRECT& rcSlotPos			= pDummyControl->GetGlobalPos();
			const UIRECT& rcSlotLocalPos	= pDummyControl->GetLocalPos();

			pSlot->SetLocalPos(rcSlotLocalPos);
			pSlot->SetGlobalPos(rcSlotPos);

			if (pSlot->IsHaveData())
				pSlot->SetVisibleSingle(TRUE);
		}
	}
}

void CGlobalRankingPage::ResetAllSlotRender(int nTotal)
{
	if (nTotal < 0) return;

	for (int i = 0; i < nTotal; i++)
	{
		CGlobalRankingPageSlot* pSlot = m_pSlot_[i];
		if (pSlot)
			pSlot->SetVisibleSingle(FALSE);
	}
}

void CGlobalRankingPage::RESET()
{
	for (int i = 0; i < MAX_TOP_RANK; i++)
		m_pSlot_[i]->RESET();

	m_nState = 0;
	m_pScrollBar->GetThumbFrame()->SetState(m_nState, MAX_ONE_VIEW_RANK);
}

void CGlobalRankingPage::SetRankingKill(WORD wPart, STOP_RANK_KILL sTopKill, int nIndex)
{
	int nRank = (wPart * 10) + nIndex;
	m_pSlot_[nRank]->SetRankingKill(sTopKill);

	m_nState++;
	m_pScrollBar->GetThumbFrame()->SetState(m_nState, MAX_ONE_VIEW_RANK);
}

void CGlobalRankingPage::SetRankingRich(WORD wPart, STOP_RANK_RICH sTopRich, int nIndex)
{
	int nRank = (wPart * 10) + nIndex;
	m_pSlot_[nRank]->SetRankingRich(sTopRich);

	m_nState++;
	m_pScrollBar->GetThumbFrame()->SetState(m_nState, MAX_ONE_VIEW_RANK);
}

void CGlobalRankingPage::SetRankingGuild(WORD wPart, STOP_RANK_GUILD sTopGuild, int nIndex)
{
	int nRank = (wPart * 10) + nIndex;
	m_pSlot_[nRank]->SetRankingGuild(sTopGuild);

	m_nState++;
	m_pScrollBar->GetThumbFrame()->SetState(m_nState, MAX_ONE_VIEW_RANK);
}