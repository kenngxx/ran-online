#pragma	once

#include "UIWindowEx.h"
#include "InventoryPage.h"
#include "InventoryPageWear.h"
#include "InventoryPageWearEx.h"
class	CInventoryPage;
class	CInventoryPageWear;
class	CInventoryPageWearEx;
class	CBasicTextButton;

class	CInventoryWindow : public CUIWindowEx
{
protected:
	enum
	{
		INVENTORY_MONEY_BUTTON = ET_CONTROL_NEXT,
		INVENTORY_PAGE,
		INVENTORY_PAGEWEAR,
		INVENTORY_PAGEWEAR_EX,
		INVENTORY_SORT_BUTTON,
		INVENTORY_VNGAINSYS_BUTTON
	};

public:
	CInventoryPage*		GetInventoryPage()	{ return m_pPage; }
private:
	CInventoryPage*		m_pPage;
	CInventoryPageWear*	m_pPageWear;
	CInventoryPageWearEx* m_pPageWearEx;

	CBasicTextBox*		m_pMoneyTextBox;

	WORD	m_wSplitItemPosX;
	WORD	m_wSplitItemPosY;
	BOOL	m_bSplitItem;

	BOOL	m_bRClick;
	BOOL	m_bRingSwap;
	BOOL	m_bEarringSwap;

	int		m_nONE_VIEW_SLOT;

public:
	CInventoryWindow ();
	virtual	~CInventoryWindow ();

public:
	void	CreateSubControl ();

public:
	void	GetSplitPos ( WORD* pwPosX, WORD* pwPosY );
	void	SetSplitPos ( WORD wPosX, WORD wPosY );

	void	SetArmSwapTabButton( BOOL bArmSub );
	CBasicTextButton*	CreateTextButton24 ( const char* szButton, UIGUID ControlID, const char* szText );
	CBasicTextButton*	CreateTextButton23 ( const char* szButton, UIGUID ControlID, const char* szText );
	float fSortTimer;
	CBasicTextButton*   pButtonRank;

public:
	virtual	void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );
	virtual	void TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg );
	virtual	void SetVisibleSingle ( BOOL bVisible );

private:
	void	SetMoney ( LONGLONG Money );

public:
	void	SetOneViewSlot ( const int& nONE_VIEW_SLOT );
	const int& GetOneViewSlot () const						{ return m_nONE_VIEW_SLOT; }
};