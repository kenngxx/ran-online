#pragma	once

#include "../Lib_Engine/GUInterface/UIGroup.h"

class CPKComboType;
class CPKComboDisplay : public CUIGroup
{
public:
	enum
	{
		FIRST_BLOOD = NO_ID + 1,
		DOUBLE_KILL,
		TRIPLE_KILL,
		ULTRA_KILL,
		KILLING_SPREE,
		MEGA_KILL,
		DOMINATING,
		GODLIKE,
		MONSTER_KILL,
		UNSTOPPABLE,
		OWNAGE,
		RAMPAGE,
		HOLYSHIT,
		WHICKEDSICK,
		COMBOWHORE,

		MAX_KILL = 15
	};

public:
	CPKComboDisplay ();
	virtual ~CPKComboDisplay();

public:
	void	CreateSubControl ();

public:
	virtual void Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

public:
	bool	START ( UIGUID cID );
	bool	RESET ( UIGUID cID );
	bool	STOP ();

	void	ALLSTOP ();

public:
	bool	KEEP_START ( UIGUID cID );
	void	KEEP_STOP ();

private:
	CPKComboType*	m_pKILL_COUNT[MAX_KILL];
	CUIControl*		m_pPositionControl;	
};