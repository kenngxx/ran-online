#include "StdAfx.h"
#include "PersonalLockerWindow.h"
#include "BasicTextButton.h"
#include "GameTextControl.h"
#include "BasicTextBoxEx.h"
#include "UITextControl.h"
#include "BasicLineBox.h"
#include "OuterInterface.h"
#include "ModalWindow.h"
#include "s_NetClient.h"
#include "RANPARAM.h"
#include "DxGlobalStage.h"
#include "DxInputString.h"
#include "DebugSet.h"



#include "ItemImage.h"
#include "GLGaeaClient.h"
#include "GLItemMan.h"
#include "InnerInterface.h"
#include "ItemMove.h"
#include "BasicButton.h"
#include "GameTextControl.h" 
#include "UITextControl.h"
#include "BasicLineBox.h"

#include "../Lib_Engine/Common/StringUtils.h"
#include "../Lib_Engine/DxCommon/DxFontMan.h"
#include "../Lib_ClientUI/Interface/UIEditBoxMan.h"
#include "../Lib_Engine/GUInterface/UIKeyCheck.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int	CPersonalLockerWindow::nLIMIT_ID = 12;
int	CPersonalLockerWindow::nLIMIT_PW2 = 12;
int	CPersonalLockerWindow::nLIMIT_CP = 7;

CPersonalLockerWindow::CPersonalLockerWindow ()
	: m_pRandTextBox(NULL)
	, m_nRandPassNumber(0)
	, m_nRPUpdateCnt(0)
	/*equipment lock, Juver, 2018/01/16 */
	,m_pButtonEquipmentLock(NULL)
	, m_pButtonEquipmentUnlock(NULL)
	/*inventory lock, Nell, 2020/01/16 */
	, m_pButtonInventoryLock(NULL)
	, m_pButtonInventoryUnlock(NULL)
	/*storage lock, Nell, 2020/01/16 */
	, m_pButtonStorageLock(NULL)
	, m_pButtonStorageUnlock(NULL)


	/*inventory lock, Nell, 2020/01/16 */
	, m_pButtonEquipmentAllinLock(NULL)
	, m_pButtonInventoryAllinLock(NULL)	
	, m_pButtonStorageAllinLock(NULL)
{
	memset( m_szRandomString, 0, sizeof( m_szRandomString ) );
}

CPersonalLockerWindow::~CPersonalLockerWindow ()
{
}

void CPersonalLockerWindow::CreateSubControl ()
{


	/*equipment lock, Juver, 2018/01/16 */
	m_pButtonEquipmentLock = new CBasicButton;
	m_pButtonEquipmentLock->CreateSub ( this, "INVEN_BUTTON_EQUIPMENT_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_EQUIPMENT_LOCK );
	m_pButtonEquipmentLock->CreateFlip ( "INVEN_BUTTON_EQUIPMENT_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonEquipmentLock->SetFlip(false);
	m_pButtonEquipmentLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonEquipmentLock );

	m_pButtonEquipmentUnlock = new CBasicButton;
	m_pButtonEquipmentUnlock->CreateSub ( this, "INVEN_BUTTON_EQUIPMENT_UNLOCK", UI_FLAG_DEFAULT, BUTTON_KEY_EQUIPMENT_UNLOCK );
	m_pButtonEquipmentUnlock->CreateFlip ( "INVEN_BUTTON_EQUIPMENT_UNLOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonEquipmentUnlock->SetFlip(false);
	m_pButtonEquipmentUnlock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonEquipmentUnlock );

	/*inventory lock, Nell, 2020/01/16 */
	m_pButtonInventoryLock = new CBasicButton;
	m_pButtonInventoryLock->CreateSub ( this, "INVEN_BUTTON_INVENTORY_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_INVENTORY_LOCK );
	m_pButtonInventoryLock->CreateFlip ( "INVEN_BUTTON_INVENTORY_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonInventoryLock->SetFlip(false);
	m_pButtonInventoryLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonInventoryLock );

	m_pButtonInventoryUnlock = new CBasicButton;
	m_pButtonInventoryUnlock->CreateSub ( this, "INVEN_BUTTON_INVENTORY_UNLOCK", UI_FLAG_DEFAULT, BUTTON_KEY_INVENTORY_UNLOCK );
	m_pButtonInventoryUnlock->CreateFlip ( "INVEN_BUTTON_INVENTORY_UNLOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonInventoryUnlock->SetFlip(false);
	m_pButtonInventoryUnlock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonInventoryUnlock );
	/*storage lock, Nell, 2020/01/16 */
	m_pButtonStorageLock = new CBasicButton;
	m_pButtonStorageLock->CreateSub ( this, "INVEN_BUTTON_STORAGE_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_STORAGE_LOCK );
	m_pButtonStorageLock->CreateFlip ( "INVEN_BUTTON_STORAGE_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonStorageLock->SetFlip(false);
	m_pButtonStorageLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonStorageLock );

	m_pButtonStorageUnlock = new CBasicButton;
	m_pButtonStorageUnlock->CreateSub ( this, "INVEN_BUTTON_STORAGE_UNLOCK", UI_FLAG_DEFAULT, BUTTON_KEY_STORAGE_UNLOCK );
	m_pButtonStorageUnlock->CreateFlip ( "INVEN_BUTTON_STORAGE_UNLOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonStorageUnlock->SetFlip(false);
	m_pButtonStorageUnlock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonStorageUnlock );
////////////////////////////////////////////////
		/*allin lock, Nell, 2020/01/16 */
	m_pButtonEquipmentAllinLock = new CBasicButton;
	m_pButtonEquipmentAllinLock->CreateSub ( this, "INVEN_BUTTON_EQUIPMENT_ALL_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_EQUIPMENT_ALL_LOCK );
	m_pButtonEquipmentAllinLock->CreateFlip ( "INVEN_BUTTON_EQUIPMENT_ALL_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonEquipmentAllinLock->SetFlip(false);
	m_pButtonEquipmentAllinLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonEquipmentAllinLock );

	

	m_pButtonInventoryAllinLock = new CBasicButton;
	m_pButtonInventoryAllinLock->CreateSub ( this, "INVEN_BUTTON_INVENTORY_ALL_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_INVENTORY_ALL_LOCK );
	m_pButtonInventoryAllinLock->CreateFlip ( "INVEN_BUTTON_INVENTORY_ALL_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonInventoryAllinLock->SetFlip(false);
	m_pButtonInventoryAllinLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonInventoryAllinLock );



	m_pButtonStorageAllinLock = new CBasicButton;
	m_pButtonStorageAllinLock->CreateSub ( this, "INVEN_BUTTON_STORAGE_ALL_LOCK", UI_FLAG_DEFAULT, BUTTON_KEY_STORAGE_ALL_LOCK );
	m_pButtonStorageAllinLock->CreateFlip ( "INVEN_BUTTON_STORAGE_ALL_LOCK_F", CBasicButton::CLICK_FLIP );
	m_pButtonStorageAllinLock->SetFlip(false);
	m_pButtonStorageAllinLock->SetVisibleSingle ( FALSE );
	RegisterControl ( m_pButtonStorageAllinLock );

}	

void CPersonalLockerWindow::Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl )
{	
	CUIWindow::Update ( x, y, LB, MB, RB, nScroll, fElapsedTime, bFirstControl );

	

	/*puton lock, Juver, 2018/01/13 */
	BOOL bEnabledE = GLGaeaClient::GetInstance().GetCharacter()->m_bEnableEquipmentLock;
	BOOL bLockedE = GLGaeaClient::GetInstance().GetCharacter()->m_bEquipmentLockStatus;
	/*inventory button lock, Nell, 2020/01/13 */
	BOOL bEnabledI = GLGaeaClient::GetInstance().GetCharacter()->m_bEnableInventoryLock;
	BOOL bLockedI = GLGaeaClient::GetInstance().GetCharacter()->m_bInventoryLockStatus;
	/*storage button lock, Nell, 2020/01/13 */
	BOOL bEnabledS = GLGaeaClient::GetInstance().GetCharacter()->m_bEnableStorageLock;
	BOOL bLockedS = GLGaeaClient::GetInstance().GetCharacter()->m_bStorageLockStatus;


	/*allin button lock, Nell, 2020/01/13 */
	if ( m_pButtonEquipmentAllinLock )	m_pButtonEquipmentAllinLock->SetVisibleSingle( !bEnabledE  );
	if ( m_pButtonInventoryAllinLock )	m_pButtonInventoryAllinLock->SetVisibleSingle( !bEnabledI  );
	if ( m_pButtonStorageAllinLock )	m_pButtonStorageAllinLock->SetVisibleSingle( !bEnabledS  );


	
	/*equipment button lock, Nell, 2020/01/13 */
	if ( m_pButtonEquipmentLock )	m_pButtonEquipmentLock->SetVisibleSingle( bEnabledE && bLockedE );
	if ( m_pButtonEquipmentUnlock )	m_pButtonEquipmentUnlock->SetVisibleSingle( bEnabledE && !bLockedE );

	/*inventory button lock, Nell, 2020/01/13 */
	if ( m_pButtonInventoryLock )	m_pButtonInventoryLock->SetVisibleSingle( bEnabledI && bLockedI );
	if ( m_pButtonInventoryUnlock )	m_pButtonInventoryUnlock->SetVisibleSingle( bEnabledI && !bLockedI );

	/*storage button lock, Nell, 2020/01/13 */
	if ( m_pButtonStorageLock )	m_pButtonStorageLock->SetVisibleSingle( bEnabledS && bLockedS );
	if ( m_pButtonStorageUnlock )	m_pButtonStorageUnlock->SetVisibleSingle( bEnabledS && !bLockedS );
}

void CPersonalLockerWindow::TranslateUIMessage ( UIGUID ControlID, DWORD dwMsg )
{
	CUIWindow::TranslateUIMessage ( ControlID, dwMsg );

	switch ( ControlID )
	{
		/*Allin button lock, Nell, 2020/01/13 */
	case BUTTON_KEY_EQUIPMENT_ALL_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Register Equipment Locker", NS_UITEXTCOLOR::WHITE  );				
					}
						if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
						CInnerInterface::GetInstance().PrintConsoleTextDlg ( "Need card to enabled this feature." );
				}
				
		}break;
	

	case BUTTON_KEY_INVENTORY_ALL_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Register Inventory Locker", NS_UITEXTCOLOR::WHITE  );				
					}
					if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
					CInnerInterface::GetInstance().PrintConsoleTextDlg ( "Need card to enabled this feature." );
				}
				
		}break;


	case BUTTON_KEY_STORAGE_ALL_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Register Storage Locker", NS_UITEXTCOLOR::WHITE  );				
					}
					if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
					CInnerInterface::GetInstance().PrintConsoleTextDlg ( "Need card to enabled this feature." );				
				}
				
		}break;
		////////////////////////////////////////////////////////

		/*puton lock, Juver, 2018/01/13 */
	case BUTTON_KEY_EQUIPMENT_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Equipment Lock", NS_UITEXTCOLOR::RED  );				
					}
				}
		}
	case BUTTON_KEY_EQUIPMENT_UNLOCK:
		{
			
				if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Equipment Unlock", NS_UITEXTCOLOR::WHITE  );				
					}
			if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
				CInnerInterface::GetInstance().OpenEquipmentLockInput();
				}
		}break;

	/*inventory button lock, Nell, 2020/01/13 */
	case BUTTON_KEY_INVENTORY_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Inventory Lock", NS_UITEXTCOLOR::RED  );				
					}
				}
		}
	case BUTTON_KEY_INVENTORY_UNLOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Inventory Unlock", NS_UITEXTCOLOR::WHITE  );				
					}
			if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
				CInnerInterface::GetInstance().OpenInventoryLockInput();
				}
		}break;

	/*storage button lock, Nell, 2020/01/13 */
	case BUTTON_KEY_STORAGE_LOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Storage Lock", NS_UITEXTCOLOR::RED  );				
					}
				}
		}
	case BUTTON_KEY_STORAGE_UNLOCK:
		{
			if ( CHECK_MOUSE_IN ( dwMsg ) )
				{
					{
					CInnerInterface::GetInstance().SHOW_COMMON_LINEINFO( "Storage Unlock", NS_UITEXTCOLOR::WHITE  );				
					}
			if( CHECK_MOUSEIN_LBUPLIKE( dwMsg ) )
				CInnerInterface::GetInstance().OpenStorageLockInput();
				}
		}break;
	};
}


