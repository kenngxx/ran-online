/*!
 * \file ActivityDisplay.h
 *
 * \author Francis Durante
 * \date Feb 15 2021
 *
 * 
 */

#pragma once

#include "../Lib_Engine/GUInterface/UIGroup.h"
class CBasicLineBoxEx;

class	CBasicTextBox;

class	CActivityDisplay : public CUIGroup
{
public:
	CActivityDisplay ();
	virtual	~CActivityDisplay ();

public:
	void	CreateSubControl ();

public:
	void	AddText ( CString strMessage, D3DCOLOR dwMessageColor );

public:
	virtual void	Update ( int x, int y, BYTE LB, BYTE MB, BYTE RB, int nScroll, float fElapsedTime, BOOL bFirstControl );

private:
	CBasicTextBox*	m_pTextBox;	
	CBasicLineBoxEx*	m_pBackground;

private:
	float	m_fLifeTime;
};