USE [master]
GO
/****** Object:  Database [RanShop]    Script Date: 7/13/2022 8:11:28 AM ******/
CREATE DATABASE [RanShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RanShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RanShop.mdf' , SIZE = 16384KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RanShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RanShop_log.ldf' , SIZE = 24384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RanShop] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RanShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RanShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RanShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RanShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RanShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RanShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [RanShop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RanShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RanShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RanShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RanShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RanShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RanShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RanShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RanShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RanShop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RanShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RanShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RanShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RanShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RanShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RanShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RanShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RanShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RanShop] SET  MULTI_USER 
GO
ALTER DATABASE [RanShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RanShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RanShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RanShop] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [RanShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RanShop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [RanShop] SET QUERY_STORE = OFF
GO
USE [RanShop]
GO
/****** Object:  Table [dbo].[ShopItemMap]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShopItemMap](
	[ProductNum] [int] IDENTITY(1,1) NOT NULL,
	[ItemMain] [int] NULL,
	[ItemSub] [int] NULL,
	[ItemName] [varchar](100) NULL,
	[ItemSec] [int] NULL,
	[ItemPrice] [varchar](100) NULL,
	[Itemstock] [varchar](100) NULL,
	[ItemCtg] [int] NULL,
	[Itemexp] [varchar](100) NULL,
	[ItemIco] [varchar](100) NULL,
	[ItemSS] [varchar](100) NULL,
	[date] [datetime] NOT NULL,
	[ItemComment] [varchar](50) NULL,
	[ItemDisc] [varchar](50) NULL,
	[IsUnli] [int] NOT NULL,
	[hidden] [int] NOT NULL,
	[ItemBought] [int] NULL,
	[isHide] [int] NOT NULL,
 CONSTRAINT [PK_ShopItemMap] PRIMARY KEY CLUSTERED 
(
	[ProductNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShopPurchase]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShopPurchase](
	[PurKey] [bigint] IDENTITY(1,1) NOT NULL,
	[UserUID] [varchar](20) NOT NULL,
	[ProductNum] [int] NOT NULL,
	[PurPrice] [int] NOT NULL,
	[PurFlag] [int] NOT NULL,
	[PurDate] [datetime] NOT NULL,
	[PurChgDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShopPurFlag]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShopPurFlag](
	[PurFlag] [int] NOT NULL,
	[PurFlagName] [varchar](20) NULL,
 CONSTRAINT [PK_ShopPurFlag] PRIMARY KEY CLUSTERED 
(
	[PurFlag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[viewShopPurchase]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------------------------------------
-- viewShopPurchase
-------------------------------------------------------------------------------

CREATE VIEW [dbo].[viewShopPurchase]
AS
SELECT   dbo.ShopPurchase.PurKey, dbo.ShopPurchase.UserUID, 
         dbo.ShopPurchase.ProductNum, dbo.ShopPurchase.PurPrice, 
         dbo.ShopPurchase.PurFlag, dbo.ShopPurchase.PurDate, 
         dbo.ShopPurchase.PurChgDate, dbo.ShopItemMap.ItemMain, 
         dbo.ShopItemMap.ItemSub, dbo.ShopItemMap.ItemName, 
         dbo.ShopPurFlag.PurFlagName
FROM     dbo.ShopPurchase INNER JOIN
         dbo.ShopItemMap ON 
         dbo.ShopPurchase.ProductNum = dbo.ShopItemMap.ProductNum LEFT OUTER JOIN
         dbo.ShopPurFlag ON dbo.ShopPurchase.PurFlag = dbo.ShopPurFlag.PurFlag













GO
/****** Object:  View [dbo].[viewShopPurchaseItem]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------------------------------------
-- view_ShopPurchaseItem.sql
-------------------------------------------------------------------------------

CREATE VIEW [dbo].[viewShopPurchaseItem]
AS
SELECT TOP 100 PERCENT SUM(A.PurPrice) AS tPrice, A.ItemMain, A.ItemSub, B.ItemName
FROM   dbo.viewShopPurchase A INNER JOIN
       dbo.ShopItemMap B ON A.ProductNum = B.ProductNum
GROUP BY A.ItemMain, A.ItemSub, B.ItemName
ORDER BY A.ItemMain, A.ItemSub













GO
/****** Object:  View [dbo].[viewShopItemMap]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[viewShopItemMap]
AS
SELECT     ProductNum, ItemMain, ItemSub, ItemPrice, Itemstock, ItemCtg, ItemSec, isHide
FROM         dbo.ShopItemMap













GO
/****** Object:  Table [dbo].[ItemListTemp]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemListTemp](
	[ProductNum] [int] NULL,
	[MainID] [int] NULL,
	[SubID] [int] NULL,
	[StrName] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemMallLogBuy]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemMallLogBuy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[ProductNum] [varchar](50) NULL,
	[PremPoints] [varchar](50) NULL,
	[AfterPremPoints] [varchar](50) NULL,
	[VotePoints] [varchar](50) NULL,
	[AfterVotePoints] [varchar](50) NULL,
	[DateBought] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogAction]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogAction](
	[ActionNum] [bigint] IDENTITY(1,1) NOT NULL,
	[ChaNum] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[TargetNum] [int] NOT NULL,
	[TargetType] [int] NOT NULL,
	[BrightPoint] [int] NOT NULL,
	[LifePoint] [int] NOT NULL,
	[ExpPoint] [money] NOT NULL,
	[ActionMoney] [money] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LogAction] PRIMARY KEY CLUSTERED 
(
	[ActionNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogShopPurchase]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogShopPurchase](
	[PurNum] [bigint] IDENTITY(1,1) NOT NULL,
	[PurKey] [varchar](21) NOT NULL,
	[PurFlag] [int] NULL,
	[PurDate] [datetime] NULL,
 CONSTRAINT [PK_LogShopPurchase] PRIMARY KEY CLUSTERED 
(
	[PurNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShopItemMap2]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShopItemMap2](
	[ProductNum] [bigint] NOT NULL,
	[ItemMain] [int] NULL,
	[ItemSub] [int] NULL,
	[ItemName] [varchar](100) NULL,
	[ItemList] [smallint] NULL,
	[Duration] [varchar](50) NULL,
	[Category] [int] NULL,
	[ItemStock] [int] NOT NULL,
	[ItemImage] [varchar](300) NULL,
	[ItemMoney] [int] NULL,
	[ItemComment] [varchar](50) NULL,
	[ItemDuration] [varchar](50) NULL,
	[ItemCategory] [int] NULL,
	[ItemLevel] [varchar](50) NULL,
	[ItemPurchased] [int] NOT NULL,
	[ItemStocks] [int] NOT NULL,
	[ItemType] [int] NULL,
 CONSTRAINT [PK_ShopItemMap2] PRIMARY KEY CLUSTERED 
(
	[ProductNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_LogShopPurchase_PurKey]    Script Date: 7/13/2022 8:11:28 AM ******/
CREATE NONCLUSTERED INDEX [IX_LogShopPurchase_PurKey] ON [dbo].[LogShopPurchase]
(
	[PurKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_ChaNum]  DEFAULT ((0)) FOR [ChaNum]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_TargetNum]  DEFAULT ((0)) FOR [TargetNum]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_TargetType]  DEFAULT ((0)) FOR [TargetType]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_BrightPoint]  DEFAULT ((0)) FOR [BrightPoint]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_LifePoint]  DEFAULT ((0)) FOR [LifePoint]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_ExpPoint]  DEFAULT ((0)) FOR [ExpPoint]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_ActionMoney]  DEFAULT ((0)) FOR [ActionMoney]
GO
ALTER TABLE [dbo].[LogAction] ADD  CONSTRAINT [DF_LogAction_ActionDate]  DEFAULT (getdate()) FOR [ActionDate]
GO
ALTER TABLE [dbo].[LogShopPurchase] ADD  CONSTRAINT [DF_LogShopPurchase_PurDate]  DEFAULT (getdate()) FOR [PurDate]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF_ShopItemMap_ItemList]  DEFAULT ((1)) FOR [ItemSec]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF__ShopItemM__ItemS__38996AB5]  DEFAULT ((0)) FOR [Itemstock]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF_ShopItemMap_date]  DEFAULT (getdate()) FOR [date]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF_ShopItemMap_IsUnli]  DEFAULT ((0)) FOR [IsUnli]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF_ShopItemMap_hidden]  DEFAULT ((1)) FOR [hidden]
GO
ALTER TABLE [dbo].[ShopItemMap] ADD  CONSTRAINT [DF_ShopItemMap_isHide]  DEFAULT ((0)) FOR [isHide]
GO
ALTER TABLE [dbo].[ShopItemMap2] ADD  CONSTRAINT [DF_ShopItemMap_ProductNum2]  DEFAULT ((1)) FOR [ProductNum]
GO
ALTER TABLE [dbo].[ShopItemMap2] ADD  CONSTRAINT [DF_ShopItemMap_ItemList2]  DEFAULT ((1)) FOR [ItemList]
GO
ALTER TABLE [dbo].[ShopItemMap2] ADD  CONSTRAINT [DF__ShopItemM__ItemS__38996AB52]  DEFAULT ((0)) FOR [ItemStock]
GO
ALTER TABLE [dbo].[ShopItemMap2] ADD  CONSTRAINT [DF_ShopItemMap_ItemPurchased2]  DEFAULT ((0)) FOR [ItemPurchased]
GO
ALTER TABLE [dbo].[ShopItemMap2] ADD  CONSTRAINT [DF__ShopItemM__ItemS__400582532]  DEFAULT ((0)) FOR [ItemStocks]
GO
ALTER TABLE [dbo].[ShopPurchase] ADD  CONSTRAINT [DF_ShopPurchase_PurPrice]  DEFAULT ((0)) FOR [PurPrice]
GO
ALTER TABLE [dbo].[ShopPurchase] ADD  CONSTRAINT [DF_ShopPurchase_PurFlag]  DEFAULT ((0)) FOR [PurFlag]
GO
ALTER TABLE [dbo].[ShopPurchase] ADD  CONSTRAINT [DF_ShopPurchase_PurDate]  DEFAULT (getdate()) FOR [PurDate]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertItem]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_InsertItem]
	@PurKey	varchar(100),
	@UserId varchar(50),
	@MID int,
	@SID int,
	@nReturn int OUTPUT
AS	
/*
Created by PrinceOfPersia
August 8, 2011
*/
DECLARE	@productNum int;

SET NOCOUNT ON;
	
BEGIN TRAN	

SET @nReturn = -1;
	
SELECT @productNum  = ProductNum FROM ShopItemMap (NOLOCK) 
WHERE ItemMain = @MID and ItemSub = @SID;

INSERT INTO ShopPurchase 
(
UserUID,
ProductNum,
PurFlag,
PurDate,
PurChgDate
)
VALUES
(
@UserId,
@productNum,
0,
GETDATE(),
GETDATE()
)

IF @@ERROR = 0 
	BEGIN 
		COMMIT TRAN;
		SET @nReturn = 0;
	END

SET NOCOUNT OFF;

RETURN @nReturn













GO
/****** Object:  StoredProcedure [dbo].[sp_InsertItem2Bank]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_InsertItem2Bank]
	@PurKey	varchar(22),
	@UserId int,
	@nReturn int OUTPUT
AS	
/*
Created by ejsayaaaa
March 23, 2018
*/
	DECLARE
		@USERNAME varchar(50),
		@currency int,
		@nUserNum int,
		@price int,
		@pp int,
		@vp int,
		@afterpp int,
		@aftervp int,
        @rowcount_var int

SET NOCOUNT ON;

SET @nReturn = -1;
SELECT @nUserNum = UserNum , @USERNAME = UserName , @pp = UserPoint , @vp = UserPoint2
From RanUser.dbo.UserInfo 
where UserNum = @UserId


	IF  @nUserNum != 0
	BEGIN
		SELECT @currency=ItemSec , @price=ItemPrice
		FROM ShopItemMap 
		WHERE ProductNum=@purkey
		IF  @currency = 1
		BEGIN
			if @vp >= @price
			begin
				if @vp !< 0
				begin
				UPDATE RanUser.dbo.UserInfo SET UserPoint2 = UserPoint2 - @price Where UserNum = @UserId
				UPDATE ShopItemMap SET Itemstock = Itemstock - 1 WHERE ProductNum=@purkey	
INSERT INTO ShopPurchase 
(
UserUID,
ProductNum,
PurFlag,
PurDate,
PurChgDate
)
VALUES
(
@USERNAME,
@PurKey,
0,
GETDATE(),
GETDATE()
)
INSERT INTO ItemMallLogBuy 
(
UserID,
ProductNum,
PremPoints,
AfterPremPoints,
VotePoints,
AfterVotePoints,
DateBought
)
VALUES
(
@USERNAME,
@PurKey,
@pp,
@pp,
@vp,
@vp-@price,
GETDATE()
)

SET @nReturn = 1;
RETURN @nReturn;
				end
			end
			else
			begin
				SET @nReturn = 0;
				RETURN @nReturn;
			end
		END
		
		IF @currency = 2
		BEGIN
			if @pp >= @price
			begin
				if @pp !< 0
				begin
					UPDATE RanUser.dbo.UserInfo SET UserPoint = UserPoint - @price Where UserNum = @UserId
					UPDATE ShopItemMap SET Itemstock = Itemstock - 1 WHERE ProductNum=@purkey
				
INSERT INTO ShopPurchase 
(
UserUID,
ProductNum,
PurFlag,
PurDate,
PurChgDate
)
VALUES
(
@USERNAME,
@PurKey,
0,
GETDATE(),
GETDATE()
)
INSERT INTO ItemMallLogBuy 
(
UserID,
ProductNum,
PremPoints,
AfterPremPoints,
VotePoints,
AfterVotePoints,
DateBought
)
VALUES
(
@USERNAME,
@PurKey,
@pp,
@pp-@price,
@vp,
@vp,
GETDATE()
)
SET @nReturn = 1;
RETURN @nReturn;
				end
			end
			else
			begin
				SET @nReturn = 0;
				RETURN @nReturn;
			end
		END
		ELSE
		BEGIN
			SET @nReturn = 0;
			RETURN @nReturn;
		END
	END
	ELSE
		BEGIN
		SET @nReturn = 0;
		RETURN @nReturn;
		END


SET NOCOUNT OFF;

RETURN @nReturn














GO
/****** Object:  StoredProcedure [dbo].[sp_LogAction_Insert]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_LogAction_Insert]
	@nChaNum int,
	@nType int,
    @nTargetNum int,
    @nTargetType int,
    @nExpPoint money,
    @nBrightPoint int,
    @nLifePoint int,
    @nMoney money
AS	
	SET NOCOUNT ON

	INSERT INTO LogAction (ChaNum,   Type,   TargetNum,   TargetType,   ExpPoint,   BrightPoint,   LifePoint,   ActionMoney) 
    VALUES    (@nChaNum, @nType, @nTargetNum, @nTargetType, @nExpPoint, @nBrightPoint, @nLifePoint, @nMoney)

	SET NOCOUNT OFF












GO
/****** Object:  StoredProcedure [dbo].[sp_purchase_change_state]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_purchase_change_state]
	@purkey varchar(22),
    @purflag int,
	@nReturn int OUTPUT
AS
	DECLARE 	
		@error_var int, -- Declare variables used in error checking.
		@rowcount_var int,
		@nFlag int

	SET NOCOUNT ON
	
	SET @nFlag = 0
	
	SELECT @nFlag=PurFlag 
	FROM ShopPurchase 
	WHERE PurKey=@purkey
	
	IF @error_var <> 0 OR @rowcount_var = 0
	BEGIN
	    SET @nReturn = 0
	    SET NOCOUNT OFF
	    RETURN @nReturn	
	END
	
	-- 官操妨绰 内靛客 泅犁 内靛啊 鞍篮 版快 俊矾
	IF @nFlag = @purflag
	BEGIN
	    SET @nReturn = 0
	    SET NOCOUNT OFF
	    RETURN @nReturn	
	END
	
	-- 沥犬窍霸 官曹荐 乐绰 版快
    UPDATE ShopPurchase
    SET PurFlag=@purflag, PurChgDate=getdate() 
    WHERE PurKey=@purkey

	SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT
	IF @error_var <> 0 OR @rowcount_var = 0
	BEGIN
	    -- 角菩
	    SET @nReturn = 0
	END
    ELSE
    BEGIN
	    -- 己傍
	    INSERT INTO LogShopPurchase (PurKey, PurFlag) 
	    VALUES (@purkey, @purflag)
	    SET @nReturn = 1
    END

	SET NOCOUNT OFF

	RETURN @nReturn












GO
/****** Object:  StoredProcedure [dbo].[sp_purchase_insert_item]    Script Date: 7/13/2022 8:11:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_purchase_insert_item]
	@purkey varchar(22),
	@useruid varchar(30),
    @productnum int,
    @purprice int,
	@nReturn int OUTPUT
AS
	DECLARE 	
		@error_var int -- Declare variables used in error checking.

	SET NOCOUNT ON

	INSERT INTO ShopPurchase (PurKey, UserUID, ProductNum, PurPrice) 
	VALUES (@purkey, @useruid, @productnum, @purprice)

	SELECT @error_var = @@ERROR
	IF @error_var <> 0 
	BEGIN
	    -- 火涝角菩
	    SET @nReturn = 0	    
	END
        ELSE
        BEGIN
	    -- 沥惑利栏肺 火涝 己傍
	    SET @nReturn = 1
        END

	SET NOCOUNT OFF

	RETURN @nReturn	












GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ShopItemMap"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 10
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewShopItemMap'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewShopItemMap'
GO
USE [master]
GO
ALTER DATABASE [RanShop] SET  READ_WRITE 
GO
